import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/', name: 'Index', component: ()=> import('../views/index.vue')},
        {path: '/FrontOffice', name: 'FrontOffice', component: ()=> import('../views/FrontOffice.vue')},
        {path: '/FrontOfficeCashier', name: 'FrontOfficeCashier', component: ()=> import('../views/FrontOfficeCashier.vue')},
        {path: '/TelephoneOperator', name: 'TelephoneOperator', component: ()=> import('../views/TelephoneOperator.vue')},
        {path: '/HouseKeeping', name: 'HouseKeeping', component: ()=> import('../views/HouseKeeping.vue')},
        {path: '/SalesCatering', name: 'SalesCatering', component: ()=> import('../views/SalesCatering.vue')},
        {path: '/PointOfSales', name: 'PointOfSales', component: ()=> import('../views/PointOfSales.vue')},
        {path: '/Outlet', name: 'Outlet', component: ()=> import('../views/Outlet.vue')},
        {path: '/NightAudit', name: 'NightAudit', component: ()=> import('../views/NightAudit.vue')},
        {path: '/IncomeAudit', name: 'IncomeAudit', component: ()=> import('../views/IncomeAudit.vue')},
        {path: '/AccountReceivable', name: 'AccountReceivable', component: ()=> import('../views/AccountReceivable.vue')},
        {path: '/Purchasing', name: 'Purchasing', component: ()=> import('../views/Purchasing.vue')},
        {path: '/Inventory', name: 'Inventory', component: ()=> import('../views/Inventory.vue')},
        {path: '/AccountPayable', name: 'AccountPayable', component: ()=> import('../views/AccountPayable.vue')},
        {path: '/GeneralCashier', name: 'GeneralCashier', component: ()=> import('../views/GeneralCashier.vue')},
        {path: '/GeneralLedger', name: 'GeneralLedger', component: ()=> import('../views/GeneralLedger.vue')},
        {path: '/FixAssets', name: 'FixAssets', component: ()=> import('../views/FixAssets.vue')},
        {path: '/Engineering', name: 'Engineering', component: ()=> import('../views/Engineering.vue')},
        {path: '/Setting', name: 'Setting', component: ()=> import('../views/Setting.vue')},
        {path: '/SystemTools', name: 'SystemTools', component: ()=> import('../views/SystemTools.vue')},
    ]
})

export default router